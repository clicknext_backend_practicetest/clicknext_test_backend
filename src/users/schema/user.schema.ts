import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument, SchemaTypes } from 'mongoose';
import { Transaction } from 'src/transactions/schema/transaction.schema';
import { Document } from 'mongoose';

export type UserDocument = HydratedDocument<User>;
@Schema({ timestamps: true })
export class User extends Document {
  @Prop()
  user_name: string;

  @Prop()
  name: string;

  @Prop()
  password: string;

  @Prop()
  balance: number;

  @Prop({ type: [{ type: SchemaTypes.ObjectId, ref: 'Transaction' }] })
  transactions: Transaction[]; // Array of Transaction objects
}

export const UserSchema = SchemaFactory.createForClass(User);
