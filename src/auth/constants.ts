import { randomBytes } from 'crypto';

export const jwtConstants = {
  secret: randomBytes(32).toString('hex'), // Generate a random 32-byte secret key
};
