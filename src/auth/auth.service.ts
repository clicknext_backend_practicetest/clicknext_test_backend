import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { User } from 'src/users/schema/user.schema';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.usersService.findOneByUsername(username);
    // console.log('enter validate');
    // console.log(user);
    const userObject = user.toObject();
    if (user && user.password === pass) {
      const { password, ...result } = userObject;
      // console.log(result);
      return result;
    }
    return null;
  }
  async login(user: User) {
    // console.log('enter login');
    // console.log(user._id);

    const payload = { username: user.user_name, sub: user._id.toString() };
    // console.log('enter payload');
    return {
      user: user,
      access_token: this.jwtService.sign(payload),
    };
  }
}
