import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { User } from 'src/users/schema/user.schema';

export type TransactionDocument = HydratedDocument<Transaction>;
@Schema({ timestamps: true })
export class Transaction {
  @Prop()
  Current: number;
  @Prop()
  Action: string;

  @Prop({ type: 'ObjectId', ref: 'User', required: true })
  sender: User; // User ID

  @Prop({ type: 'ObjectId', ref: 'User', default: null })
  receiver: User; // User ID

  @Prop()
  Amount: number;
}

export const TransactionSchema = SchemaFactory.createForClass(Transaction);
