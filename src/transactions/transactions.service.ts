import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { UpdateTransactionDto } from './dto/update-transaction.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Transaction } from './schema/transaction.schema';
import { Model } from 'mongoose';
import { User } from 'src/users/schema/user.schema';

@Injectable()
export class TransactionsService {
  constructor(
    @InjectModel(Transaction.name) private transactionModel: Model<Transaction>,
    @InjectModel(User.name) private userModel: Model<User>,
  ) {}
  async create(createTransactionDto: CreateTransactionDto) {
    console.log('enter create');

    // if (
    //   'remain' in createTransactionDto &&
    //   typeof createTransactionDto.remain === 'number' &&
    //   createTransactionDto.remain < 0
    // ) {
    //   throw new BadRequestException(
    //     'You cannot make transaction with current amount of your balance',
    //   );
    // }
    const user = await this.userModel.findById(createTransactionDto.sender);

    if (createTransactionDto.Action == 'DEPOSIT') {
      console.log('enter dep');
      console.log(typeof createTransactionDto.Amount);
      const createdTransaction = new this.transactionModel({
        Action: createTransactionDto.Action,
        Amount: createTransactionDto.Amount,
        sender: user,
        Current: user.balance + parseInt(createTransactionDto.Amount),
      });
      user.balance = createdTransaction.Current;
      user.transactions.push(createdTransaction.id);
      await user.save();
      const savedTransaction = await createdTransaction.save();
      return savedTransaction;
    } else if (createTransactionDto.Action == 'WITHDRAW') {
      // const user = await this.userModel.findByIdAndUpdate(
      //   createTransactionDto.sender,
      // );
      const createdTransaction = new this.transactionModel({
        Action: createTransactionDto.Action,
        Amount: createTransactionDto.Amount,
        sender: user,
        Current: user.balance - parseInt(createTransactionDto.Amount),
      });
      user.balance = createdTransaction.Current;
      user.transactions.push(createdTransaction.id);
      await user.save();
      const savedTransaction = await createdTransaction.save();

      return savedTransaction;
    } else if (createTransactionDto.Action == 'TRANSFER') {
      console.log('enter transfer');

      const user2 = await this.userModel.findById(
        createTransactionDto.receiver,
      );
      console.log('pass user2');

      if (!user2) {
        throw new BadRequestException('user not existing');
      }
      console.log(typeof createTransactionDto.Amount);
      const transferTransaction = new this.transactionModel({
        Action: 'TRANSFER',
        Amount: createTransactionDto.Amount,
        sender: user,
        receiver: user2,
        Current: user.balance - parseInt(createTransactionDto.Amount),
      });
      user.balance = transferTransaction.Current;
      console.log(user2);
      console.log(createTransactionDto.Amount);
      console.log(typeof createTransactionDto.Amount);
      console.log(1 + createTransactionDto.Amount);

      const receiveTransaction = new this.transactionModel({
        Action: 'RECEIVE',
        Amount: createTransactionDto.Amount,
        sender: user,
        receiver: user2,
        Current: user2.balance + parseInt(createTransactionDto.Amount),
      });
      user2.balance = receiveTransaction.Current;
      const savedTransaction = await transferTransaction.save();
      const savedTransaction2 = await receiveTransaction.save();
      user.transactions.push(savedTransaction.id);
      user2.transactions.push(savedTransaction2.id);
      await user.save();
      await user2.save();
      return [savedTransaction, savedTransaction2];
      // return [transferTransaction, receiveTransaction];
    }

    //   createTransactionDto.remain = createTransactionDto.senderRemain;
    //   createTransactionDto.side = 'Sender';
    //   const createdSenderTransaction = new this.transactionModel(
    //     createTransactionDto,
    //   );
    //   const savedSenderTransaction = await createdSenderTransaction.save();
    //   await this.userModel.findByIdAndUpdate(
    //     createTransactionDto.sender, // Assuming you have sender's ID
    //     { $push: { transactions: savedSenderTransaction } },
    //     { new: true },
    //   );
    // } else {
    //   await this.userModel.findByIdAndUpdate(
    //     createTransactionDto.sender, // Assuming you have sender's ID
    //     { $push: { transactions: savedTransaction } },
    //     { new: true },
    //   );
    // }
    // Update user's transaction array with the new transaction object
  }

  findAll() {
    return this.transactionModel
      .find()
      .populate('sender')
      .populate('receiver')
      .exec();
  }

  findOne(id: string) {
    return this.transactionModel.findById(id);
  }
  findAllBySenderId(userId: string) {
    return this.transactionModel
      .find({ sender: userId, Action: { $ne: 'RECEIVE' } })
      .populate('sender')
      .populate('receiver') // Add conditions here
      .exec(); // Execute the query
  }
  findAllByReceiverId(userId: string) {
    return this.transactionModel
      .find({ receiver: userId, Action: { $ne: 'TRANSFER' } })
      .populate('sender')
      .populate('receiver') // Add conditions here
      .exec(); // Execute the query
  }

  async update(id: string, updateTransactionDto: UpdateTransactionDto) {
    return await this.transactionModel
      .findByIdAndUpdate(id, updateTransactionDto, { new: true })
      .exec();
  }

  async remove(id: string) {
    return await this.transactionModel.findByIdAndRemove(id);
  }
}
