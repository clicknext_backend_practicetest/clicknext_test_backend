import { User } from 'src/users/schema/user.schema';

export class CreateTransactionDto {
  // Current: number;
  Action: string;
  sender: string; // User ID
  receiver?: string; // User ID
  Amount: string;
}
